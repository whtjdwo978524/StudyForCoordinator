//
//  LoginViewController.swift
//  StudyForCoordinator
//
//  Created by sungjae on 2022/12/19.
//

import Foundation
import UIKit
import SnapKit

// 로그인 버튼 누르면 Coordinator에게 알리기 위한 용도로 Delegator 생성
protocol LoginViewControllerDelegate {
  func login()
}

class LoginViewController: UIViewController {
  
  var loginButton: UIButton = {
    let button = UIButton()
    button.setTitle("로그인", for: .normal)
    button.setTitleColor(.black, for: .normal)
    button.backgroundColor = .white
    
    return button
  }()
  
  var delegate: LoginViewControllerDelegate?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    loginButton.addTarget(self, action: #selector(tapOnLoginButton), for: .touchUpInside)
    view.addSubview(loginButton)
    
    loginButton.snp.makeConstraints { make in
      make.centerX.centerY.equalToSuperview()
    }
  }
  
  @objc
  func tapOnLoginButton() {
    self.delegate?.login()
  }
}
