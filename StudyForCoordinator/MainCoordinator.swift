//
//  MainCoordinator.swift
//  StudyForCoordinator
//
//  Created by sungjae on 2022/12/19.
//

import Foundation
import UIKit

protocol MainCoordinatorDelegate {
  func didLoggedOut(coordinator: Coordinator)
}

class MainCoordinator: Coordinator, MainViewControllerDelegate {
  
  var childCoordinators: [Coordinator] = []
  
  private var naviagationController: UINavigationController!
  var delegate: MainCoordinatorDelegate?
  
  init(naviagationController: UINavigationController) {
    self.naviagationController = naviagationController
  }
  
  func start() {
    let mainVC = MainViewController()
    mainVC.view.backgroundColor = .white
    mainVC.delegate = self
    
    self.naviagationController.viewControllers = [mainVC]
  }
  
  func logout() {
    delegate?.didLoggedOut(coordinator: self)
  }
  
}

