//
//  AppCoordinator.swift
//  StudyForCoordinator
//
//  Created by sungjae on 2022/12/19.
//

import Foundation
import UIKit

protocol Coordinator: AnyObject {
  var childCoordinators: [Coordinator] { get set }
  func start()
}

class AppCoordinator: LoginCoordinatorDelegate, MainCoordinatorDelegate {
  
  var childCoordinators: [Coordinator] = []
  private var navigationController: UINavigationController!
  
  var isLoggedIn: Bool = false
  
  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
  }
  
  func start() {
    if self.isLoggedIn {
      self.showMainViewController()
    } else {
      self.showLoginViewController()
    }
  }
  
  private func showMainViewController() {
    let coordinator = MainCoordinator(naviagationController: self.navigationController)
    coordinator.delegate = self
    coordinator.start()
    
    self.childCoordinators.append(coordinator)
  }
  
  private func showLoginViewController() {
    let coordinator = LoginCoordinator(naviagationController: self.navigationController)
    coordinator.delegate = self
    coordinator.start()   // LoginVC 화면 설정 같은 초기 동작
    self.childCoordinators.append(coordinator)
  }
  
  // 로그인에 성공하면 자기 자신(LoginCoordinator)를 필터링을 통해 지우고 MainVC로 넘어가기 위함
  func didLoggedIn(coordinator: Coordinator) {
    self.childCoordinators = self.childCoordinators.filter { $0 !== coordinator }
    self.showMainViewController()
  }
  
  func didLoggedOut(coordinator: Coordinator) {
    self.childCoordinators = self.childCoordinators.filter { $0 !== coordinator }
    self.showLoginViewController()
  }
  
}
