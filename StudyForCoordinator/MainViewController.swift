//
//  MainViewController.swift
//  StudyForCoordinator
//
//  Created by sungjae on 2022/12/19.
//

import Foundation
import UIKit
import SnapKit

protocol MainViewControllerDelegate {
  func logout()
}

class MainViewController: UIViewController {
  
  var logoutButton: UIButton = {
    let button = UIButton()
    button.setTitle("로그아웃", for: .normal)
    button.setTitleColor(.red, for: .normal)
    button.backgroundColor = .white
    
    return button
  }()
  
  var delegate: MainViewControllerDelegate?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    logoutButton.addTarget(self, action: #selector(tapOnLogoutButton), for: .touchUpInside)
    view.addSubview(logoutButton)
    
    logoutButton.snp.makeConstraints { make in
      make.centerX.centerY.equalToSuperview()
    }
  }
  
  @objc
  func tapOnLogoutButton() {
    self.delegate?.logout()
  }
}
