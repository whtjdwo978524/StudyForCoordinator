//
//  LoginCoordinator.swift
//  StudyForCoordinator
//
//  Created by sungjae on 2022/12/19.
//

import Foundation
import UIKit

// AppCoordinator에게 로그인 됐다는 것을 알리기 위한 Delegate 생성
protocol LoginCoordinatorDelegate {
  func didLoggedIn(coordinator: Coordinator)
}

class LoginCoordinator: Coordinator, LoginViewControllerDelegate {
  
  var childCoordinators: [Coordinator] = []
   
  private var naviagationController: UINavigationController!
  var delegate: LoginCoordinatorDelegate?
  
  init(naviagationController: UINavigationController) {
    self.naviagationController = naviagationController
  }
  
  func start() {
    let loginVC = LoginViewController()
    loginVC.view.backgroundColor = .white
    loginVC.delegate = self
    
    self.naviagationController.viewControllers = [loginVC]
  }
  
  func login() {
    // LoginCoordinator 자신을 AppCoordinator에게 보냄
    self.delegate?.didLoggedIn(coordinator: self)
  }
}
